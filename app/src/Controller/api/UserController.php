<?php


namespace App\Controller\api;


use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{

    /**
     * @Route("/api/user/list", name="api_user_list")
     */
    public function index()
    {
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $users = $userRepository->findAll();
        $usersData = [];
        foreach ($users as $user){
            $usersData[] = $user->asArray();
        }
        return new JsonResponse($usersData);
    }

    /**
     * @Route("/api/user/view/{id}", name="api_user_view")
     */
    public function view($id)
    {
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $user = $userRepository->findOneBy(['id' => $id]);
        if ($user) {
            return new JsonResponse($user->asArray());
        }
        throw new NotFoundHttpException();
    }

    /**
     * @Route("/api/user/create/", name="api_user_create")
     */
    public function create(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $userData = json_decode($request->getContent(), true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new BadRequestHttpException();
        }

        $user = new User();
        $user->setUsername($userData['name']);
        $user->setRoles(['ROLE_USER']);
        $user->setFirstname($userData['firstname']);
        $user->setLastname($userData['lastname']);
        $user->setPhone($userData['phone']);

        $password = $encoder->encodePassword($user, $userData['password']);
        $user->setPassword($password);

        $objectManager = $this->getDoctrine()->getManager();
        $objectManager->persist($user);
        $objectManager->flush();

        return new JsonResponse($user->asArray());
    }

    /**
     * @Route("/api/user/update/{id}", name="api_user_update")
     */
    public function update($id, Request $request, UserPasswordEncoderInterface $encoder)
    {
        $userData = json_decode($request->getContent(), true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new BadRequestHttpException();
        }

        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $user = $userRepository->findOneBy(['id' => $id]);

        if ($user) {
            $user->setUsername($userData['name']);
            $user->setFirstname($userData['firstname']);
            $user->setLastname($userData['lastname']);
            $user->setPhone($userData['phone']);

            if ($userData['password']) {
                $password = $encoder->encodePassword($user, $userData['password']);
                $user->setPassword($password);
            }

            $objectManager = $this->getDoctrine()->getManager();
            $objectManager->persist($user);
            $objectManager->flush();

            return new JsonResponse($user->asArray());
        }
        throw new NotFoundHttpException();
    }

    /**
     * @Route("/api/user/delete/{id}", name="api_user_delete")
     */
    public function delete($id)
    {
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $user = $userRepository->findOneBy(['id' => $id]);
        if($user){
            $objectManager = $this->getDoctrine()->getManager();
            $objectManager->remove($user);
            $objectManager->flush();
            return new JsonResponse([
                'status' => 'success',
            ]);
        }
        throw new NotFoundHttpException();
    }
}
