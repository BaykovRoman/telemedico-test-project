<?php


namespace App\Controller;


use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/main", name="app_main")
     */
    public function index()
    {
        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $user->getUsername();
        return $this->render('app/main.html.twig', [
            'apiKey' => $user->getApiToken(),
            'currentUser' => json_encode($user->asArray()),
        ]);
    }
}
