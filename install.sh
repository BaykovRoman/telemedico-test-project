composer --working-dir=app install

docker-compose down
docker volume rm $(docker volume ls -qf dangling=true) 2>/dev/null
docker rmi $(docker images | grep "^<none>" | awk "{print $3}") 2>/dev/null
docker-compose build
docker-compose up -d

func(){
docker exec telemedico-db /bin/bash -c 'mysql --user=root telemedico -e "select 777"' 2>&1 >/dev/null
}
echo
echo -n '### waiting mysql '
while func | grep -q "ERROR\|command not found"
do
echo -n '.'
sleep 5
done
echo -n ' ready ###'
echo

TEMP_FILE_PATH="/app/docker/db/data/drop_all_tables.sql"
docker exec telemedico-db /bin/bash -c "echo 'DROP DATABASE IF EXISTS telemedico;' > $TEMP_FILE_PATH"
docker exec telemedico-db /bin/bash -c "mysql -uroot < $TEMP_FILE_PATH"
docker exec telemedico-db /bin/bash -c "rm -f $TEMP_FILE_PATH"

echo '### create database '
docker exec telemedico-app /bin/bash -c "php bin/console doctrine:database:create"
echo '### migrate '
docker exec telemedico-app /bin/bash -c "php bin/console doctrine:migrations:migrate -q"
echo '### load fixtures '
docker exec telemedico-app /bin/bash -c "php bin/console doctrine:fixtures:load -q"
echo '### done '
