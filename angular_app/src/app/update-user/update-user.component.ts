import {Component, OnInit} from '@angular/core';
import {User} from "../models/user.model";
import {UserApiService} from "../user-api.service";
import {DataServiceService} from "../data-service.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {

  id: number;
  private sub: any;
  private user: User;
  private password: string;
  private originalUsername: string;

  constructor(
    private userApi: UserApiService,
    private dataService: DataServiceService,
    private router: Router,
    private route: ActivatedRoute,) {
  }

  ngOnInit() {
    this.user = new User({});
    this.password = '';
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.loadUser(this.id);
    });
  }

  loadUser(id: number) {
    this.userApi.getUser(this.dataService.apiKey, id)
      .subscribe((data: any) => {
        this.user = new User(data);
        this.originalUsername = this.user.username;
      });
  }

  submitData() {
    let data = {
      "name": this.user.username,
      "password": this.password,
      "firstname": this.user.firstname,
      "lastname": this.user.lastname,
      "phone": this.user.phone
    };

    if (!this.user.username) {
      console.log('Username is required');
      return;
    }
    console.log(data);

    this.userApi.updateUser(this.dataService.apiKey, data, this.id)
      .subscribe((data: any) => {
        if (data && this.dataService.getCurrentUser().id == this.id) {
          let user = new User(data);
          this.dataService.setCurrentUser(user);
        }
        this.router.navigateByUrl('');
      });

  }

}
