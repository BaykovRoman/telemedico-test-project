import {Component, OnInit} from '@angular/core';
import {UserApiService} from "../user-api.service";
import {User} from "../models/user.model";
import {DataServiceService} from "../data-service.service";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  private users: User[];

  constructor(private userApi: UserApiService, private dataService: DataServiceService) {
  }

  ngOnInit() {
    this.loadUsers();
  }

  loadUsers() {
    this.userApi.getUsers(this.dataService.apiKey)
      .subscribe((data: User[]) => {
        this.users = data;
      });
  }

  removeUser(id: number) {
    this.userApi.removeUser(this.dataService.apiKey, id)
      .subscribe((data: any) => {
        this.loadUsers();
      });
  }

  getUsers() {
    return this.users;
  }

  getDataService() {
    return this.dataService;
  }
}
