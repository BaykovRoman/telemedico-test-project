export class User {
  id: number;
  username: string;
  roles: string[];
  firstname: string;
  lastname: string;
  phone: string;

  isAdmin() {
    return this.roles.find(element => element == 'ROLE_ADMIN');
  }

  constructor(data: object) {
    let jsonObj: any = data;
    for (let prop in jsonObj) {
      this[prop] = jsonObj[prop];
    }
  }
}
