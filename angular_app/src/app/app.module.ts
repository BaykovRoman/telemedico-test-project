import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NewUserComponent} from './new-user/new-user.component';
import {RouterModule, Routes} from "@angular/router";
import { UserListComponent } from './user-list/user-list.component';
import {HttpClientModule} from "@angular/common/http";
import { UserViewComponent } from './user-view/user-view.component';
import {FormsModule} from "@angular/forms";
import { UpdateUserComponent } from './update-user/update-user.component';

const appRoutes: Routes = [
  {path: 'users/new', component: NewUserComponent},
  {path: 'users/:id', component: UserViewComponent},
  {path: 'users/new', component: NewUserComponent},
  {path: 'users/update/:id', component: UpdateUserComponent},
  {path: '', component: UserListComponent},
];

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    FormsModule
  ],
  declarations: [
    AppComponent,
    NewUserComponent,
    UserListComponent,
    UserViewComponent,
    UpdateUserComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule {
}
