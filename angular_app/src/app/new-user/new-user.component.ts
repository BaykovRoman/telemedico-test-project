import {Component, OnInit} from '@angular/core';
import {User} from "../models/user.model";
import {UserApiService} from "../user-api.service";
import {DataServiceService} from "../data-service.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {

  private user: User;
  private password: string;

  constructor(
    private userApi: UserApiService,
    private dataService: DataServiceService,
    private router: Router) {
  }

  ngOnInit() {
    this.user = new User({});
  }

  submitData() {
    let data = {
      "name": this.user.username,
      "password": this.password,
      "firstname": this.user.firstname,
      "lastname": this.user.lastname,
      "phone": this.user.phone
    };

    if (!this.user.username || !this.password) {
      console.log('Username and password are required');
      return;
    }

    this.userApi.createUser(this.dataService.apiKey, data)
      .subscribe((data: any) => {
        this.router.navigateByUrl('');
      });

  }

}
