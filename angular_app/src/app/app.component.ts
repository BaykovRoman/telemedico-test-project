import {Component, ElementRef, Input} from '@angular/core';
import {DataServiceService} from "./data-service.service";
import {User} from "./models/user.model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'angular-app';

  // @Input('apiKey') apiKey: string;
  constructor(private elementRef: ElementRef, private dataService: DataServiceService) {
    dataService.apiKey = this.elementRef.nativeElement.getAttribute('apiKey');
    dataService.currentUser = new User(JSON.parse(this.elementRef.nativeElement.getAttribute('currentUser')));
  }

  getDataService() {
    return this.dataService;
  }
}
