import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpClientModule} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class UserApiService {

  constructor(private http: HttpClient) {
  }

  getUsers(apiKey: string) {
    return this.http.get(
      'api/user/list',
      this.getHttpOptions(apiKey)
    );
  }

  getUser(apiKey: string, id: number) {
    return this.http.get(
      'api/user/view/' + id,
      this.getHttpOptions(apiKey)
    );
  }

  removeUser(apiKey: string, id: number) {
    return this.http.get(
      'api/user/delete/' + id,
      this.getHttpOptions(apiKey)
    );
  }

  createUser(apiKey: string, data: object) {
    return this.http.post(
      'api/user/create/',
      data,
      this.getHttpOptions(apiKey)
    );
  }

  updateUser(apiKey: string, data: object, id: number) {
    return this.http.post(
      'api/user/update/' + id,
      data,
      this.getHttpOptions(apiKey)
    );
  }


  getHttpOptions(apiKey: string) {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-AUTH-TOKEN': apiKey
      })
    };
  }
}
