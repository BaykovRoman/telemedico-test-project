import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {User} from "../models/user.model";
import {UserApiService} from "../user-api.service";
import {DataServiceService} from "../data-service.service";

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.css']
})
export class UserViewComponent implements OnInit {

  id: number;
  private sub: any;
  private user: User;

  constructor(
    private route: ActivatedRoute,
    private userApi: UserApiService,
    private dataService: DataServiceService) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.loadUser(this.id);
    });
  }

  loadUser(id: number) {
    this.userApi.getUser(this.dataService.apiKey, id)
      .subscribe((data: any) => {
        this.user = new User(data);
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
