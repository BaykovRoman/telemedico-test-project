import {Injectable} from '@angular/core';
import {User} from "./models/user.model";

@Injectable({
  providedIn: 'root'
})
export class DataServiceService {
  apiKey: string;
  currentUser: User;

  getApiKey() {
    return this.apiKey;
  }

  getCurrentUser() {
    return this.currentUser;
  }

  setCurrentUser(user: User) {
    this.currentUser = user;
  }

  constructor() {
  }
}
